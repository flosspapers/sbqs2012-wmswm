\documentclass[12pt]{article}
\usepackage{sbc-template}
\usepackage{url}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{comment}
\usepackage{scalefnt}
\usepackage[brazilian]{babel}

\sloppy

\title{Mapeamento de Código Limpo em Métricas de Código-fonte}

\author{João Machini, Lucianna Almeida, Paulo Meirelles, Fabio Kon}
\address{%
Centro de Competência em Software Livre -- Universidade de São Paulo
\email{\{joaomm,lucianna,paulormm,fabio.kon\}@ime.usp.br}
}

\usepackage{xcolor}
\newcommand{\TODO}[1]{
  {
    \color{red}\textbf{{#1} (TODO)}
  }
}

\begin{document}

\maketitle
%-------------------------------------------------------------------------------
\vspace{-0.5em}
\begin{resumo}

Neste trabalho elencamos as ideias e conceitos elaborados por
especialistas no desenvolvimento de software orientado a objetos, buscando um
maior entendimento de boas soluções, práticas e cuidados quanto ao código-fonte.
%
Isso resultou em um mapeamento entre um conjunto de métricas de
código-fonte e os conceitos de código limpo como escolhas de bons nomes,
ausência de duplicações, organização e simplicidade de forma a facilitar a
detecção de trechos de código que poderiam receber melhorias.
%
Também, apresenta-se uma maneira de interpretar os valores das métricas
através de cenários problemáticos e suas possíveis melhorias.

\end{resumo}
%-------------------------------------------------------------------------------
\vspace{-0.5em}
\section{Introdução} \label{sec:introducao}
\vspace{-0.3em}

A indústria de software busca continuamente por melhorias nos métodos
de desenvolvimento. Do ponto de vista prático, os processos tradicionais investem tempo especificando requisitos
e planejando a arquitetura do software para que o trabalho possa ser dividido entre os
programadores, cuja função é transformar o design anteriormente concebido em código.
%
Diante de uma perspectiva dos métodos ágeis, é dado muita importância à entrega constante 
de valor ao cliente (\textit{http://agilemanifesto.org}). Nesse caso, por exemplo, se há um agente
financiador ou uma comunidade de software livre como cliente, o foco está na
entrega de funcionalidades que possam ser rapidamente colocadas no ambiente
produção e receber \textit{feedback} constante.
%
Em ambos os casos, não há como ignorar o fato de que o código-fonte da aplicação será
desenvolvido gradativamente e diferentes programadores farão alterações e extensões
continuamente. Nesse contexto, novas funcionalidades são adicionadas e falhas sanadas.

Uma situação comum durante o desenvolvimento, é um programador lidar com um trecho que
ainda não teve contato para fazer alterações. Nessa tarefa, por exemplo, o programador (i) lida com métodos extensos e classes
com muitas linhas de código que se comunicam com diversos objetos; (ii) encontra comentários
desatualizados relacionados a uma variável com nome abreviado e sem significado, e (iii) se depara com métodos que
recebem valores booleanos e possuem estruturas encadeadas complexas com muitas operações duplicadas.
%
Depois de um longo período de leitura, o desenvolvedor encontra o trecho de lógica em que deve 
fazer a mudança. A alteração frequêntemente consiste na modificação de poucas linhas e o 
desenvolvedor considera seu trabalho como terminado, sem ter feito melhorias no código confuso
com o qual teve dificuldades.

O quadro exemplificado ilustra uma circustância com aspectos a serem observados.
Primeiramente, há uma diferença significativa na proporção entre linhas lidas e as inseridas.
Para cada uma das poucas linhas que escreveu, o desenvolvedor teve que compreender diversas
outras \cite{Beck2007}. Além disso, não houve melhorias na clareza ou flexibilidade do código, fazendo 
com que os responsáveis pela próxima alteração, seja o próprio desenvolvedor ou o seguinte,
tenham que enfrentar as mesmas dificuldades.

Tendo essa situação em vista, neste trabalho apresentamos um estilo de programação baseado
no paradigma da Orientação a Objetos que busca o que denominamos de ``código limpo'', concebido e aplicado por renomados desenvolvedores de software como Robert C. Martin \cite{Martin2008} e Kent Beck \cite{Beck2007}.
%
Também, propomos uma maneira de utilizar métricas de código-fonte para auxiliar no desenvolvimento
de um código limpo. Beneficiando-se da automação da coleta de métricas e de uma maneira objetiva de interpretar seus valores,
os desenvolvedores poderão acompanhar a limpeza de seus códigos e detectar cenários problemáticos. Um melhor detalhamento dos conceitos e a aplicação deles em um estudo de caso real podem ser lidos em um relatório técnido do IME-USP(\textit{http://ccsl.ime.usp.br/codigolimpo}).

\vspace{-0.5em}
%-------------------------------------------------------------------------------
\section{Código Limpo} \label{sec:codigo-limpo}
\vspace{-0.3em}

No livro \textit{Clean Code} \cite{Martin2008}, o autor, Robert Martin, entrevistou grandes especialistas em desenvolvimento de software como Ward Cunningham (colaborador na criação do \textit{Fit}, do \textit{Wiki} e da \textit{Programação Extrema} \cite{Beck1999}) e Dave Thomas (fundador da OTI - \textit{Object Technology International} - e muito envolvido no Projeto Eclipse) questionando-os quanto a uma definição para código limpo. Cada um dos entrevistados elaborou respostas diferentes, destacando características subjetivas, como elegância, facilidade de alteração e simplicidade, e outras puramente técnicas, incluindo a falta de duplicações, presença de testes de unidade e de aceitação e a minimização do número de entidades.

Em certo sentido, um código limpo está inserido em um estilo de programação que busca a proximidade a três valores: expressividade, 
simplicidade e flexibilidade. Tais termos também são utilizados por Kent Beck no livro \textit{Implementation Patterns} \cite{Beck2007} que estão em conformidade com a unidade de pensamento que permeia as respostas dos especialistas.
%
Um importante aspecto quanto o desenvolvimento de um código limpo é o reconhecimento de que ele não será obtido em uma primeira tentativa. Robert Martin ressalta que as versões iniciais de um método, classe e outras estruturas nunca são exatamente uma boa solução. É necessário tempo e preocupação com cada elemento desde o nome escolhido para uma variável até uma hierarquia de classes.

Diante dessa realidade, queremos garantir que não teremos empecilhos para as refatorações. Um caminho para isso é concentrar a atenção no desenvolvimento de um código limpo. Por exemplo, se as variáveis estiverem devidamente nomeadas, não precisaremos criar um comentário para explicá-las. Se os métodos estiverem bem nomeados e possuírem uma única tarefa, não será necessário documentar o que são os parâmetros e o valor de retorno. E por fim, se os testes estiverem bem executados, teremos uma documentação executável do código que garante a corretude.

Robert Martin enfatiza que o desejado é não engessar o desenvolvimento e melhoria do código com dificuldades para os programadores, mas 
compreender quais elementos do software precisam de documentação e contruí-la adequadamente. Dessa forma, como um dos resultados deste trabalho é o levantamento dos aspectos importantes para termos um código limpo no que diz respeito aos detalhes quanto aos métodos e classes:

\vspace{-1.7em}
\paragraph*{-- Composição de Métodos.} Compor os métodos em chamadas para outros rigorosamente no mesmo nível de abstração abaixo. Isso resulta em (i) facilidade de entendimento de métodos menores; (ii) criação de métodos menores com nomes explicativos. Como consequências temos (i) menos operações por métodos; (ii) maior número de parâmetros da classe; (iii) mais métodos na classe, mas com menos responsabilidades.

\vspace{-1.7em}
\paragraph*{-- Métodos Explicativos.} Criar um método que encapsule uma operação pouco clara geralmente associada a um comentário. Isso leva ao ``código cliente'' do método novo ter uma operação com nome que melhor se encaixa no contexto. A consequência disso são mais métodos na classe, mas com menos responsabilidades.

\vspace{-1.7em}
\paragraph*{-- Métodos como Condicionais.} Criar um método que encapsule uma expressão booleana para obter condicionais mais claros. O que resulta (i) na facilidade na leitura de condicionais no código cliente; (ii) no encapsulamento de uma expressão booleana. Mais uma vez, a consequência é mais métodos na classe, mas com menos responsabilidades cada um.

\vspace{-1.7em}
\paragraph*{-- Evitar Estruturas Encadeadas.} Utilizar a composição de métodos para minimizar a quantidade de estruturas encadeadas em cada método. Isso resulta em (i) facilidade para a criação de testes; (ii) cada método terá estruturas mais simples e fáceis de serem compreendidas. Em suma, como consequências teremos menos estruturas encadeadas por método (o número total da classe continuará inalterado).

\vspace{-1.7em}
\paragraph*{-- Cláusulas Guarda.} Criar um retorno logo no inicio de um método ao invés da criação de estruturas encadeadas com if sem else. Isso proporciona (i) estruturas de condicionais mais simples; (ii) o leitor do código não espera por uma contrapartida do condicional (ex: if sem else). Assim, como consequência, temos menos estruturas encadeadas na classe.

\vspace{-1.7em}
\paragraph*{-- Objeto Método.} Criar uma classe que encapsule uma operação complexa simplificando a original (``cliente''). Dessa forma, (i) o ``código cliente'' terá um método bastante simples; (ii) a nova classe gerada poderá ser refatorada sem preocupações com alterações no ``código cliente''; (iii) a nova classe poderá ter testes separados. As consequências são (i) menos operações no método cliente; (ii) menos responsabilidades da classe cliente; (iii) mais classes, com menos resposbilidades cada; (iv) acoplamento da classe cliente com a nova classe.

\vspace{-1.7em}
\paragraph*{-- Evitar Flags como Argumentos.} Ao invés de criar um método que recebe uma \textit{flag} e ter diversos comportamentos, criar  um  método para cada comportamento. Isso leva (i) ao leitor do código não precisará entender um método com muitos condicionais; (ii) ter testes de unidade independentes. A consequências é ter mais métodos na classe, mas com menos responsabilidades cada um.

\vspace{-1.7em}
\paragraph*{-- Objeto como Parâmetro.} Localizar parâmetros que formam uma unidade e criar uma classe que os encapsule. Isso gera (i) um menor número de parâmetros, o que facilita os testes e a legibilidade; (ii) a criação de uma classe que poderá ser reutilizada em outras partes do sistema. Como consequências disso: (i) menos parâmetros passados pela classe; (ii) mais classes, com menos responsabilidades cada; (iii) mais acoplamento da classe cliente com a nova classe.

\vspace{-1.7em}
\paragraph*{-- Parâmetros como Variável de Instância.} Localizar parâmetro muito utilizado pelos métodos de uma classe e transformá-lo em variável de instância. Assim, não haverá a necessidade de passar longas listas de parâmetros através de todos os métodos. Como consequências temos (i) menos parâmetros passados pela classe; (ii) menor Possível diminuição na coesão.

\vspace{-1.7em}
\paragraph*{-- Uso de Exceções.} Criar um fluxo normal separado do fluxo de tratamento de erros utilizando exceções ao invés de valores de retornos e condicionais. Dessa forma, proporciona clareza do fluxo normal sem tratamento de erros através de valores de retornos e condicionais e, como consequências, menos estruturas encadeadas.

\vspace{-1.7em}
\paragraph*{-- Maximizar Coesão.} Quebrar uma classe que não segue o Princípio da Responsabilidade Única. Isso significa que (i) cada classe terá uma única responsabilidade; (ii) cada classe terá seus testes independentes; (iii) sem interferências na implementação das responsabilidades. As consequências são:
(i) mais classes, com menos responsabilidades; (ii) menos métodos em cada classe; (iii) menos atributos em cada classe.

\vspace{-1.7em}
\paragraph*{-- Delegação de Tarefa.} Transferir um método que utiliza dados de uma classe ``B'' para a ``B'' (ou seja, mesma classe). Isso leva a (i) redução do acoplamento entre as classes; (ii) proximidade dos métodos e dados sobre os quais trabalham. Dessa forma, como consequências, temos (i) menos métodos na classe inicial; (ii) mais métodos na classe que recebe o novo método.

\vspace{-1.7em}
\paragraph*{-- Objeto Centralizador.} Criar uma classe que encapsule uma operação com alta dependência entre classes. Isso proporciona (i) simplificação da classe cliente; (ii) Redução do acoplamento da classe cliente com as demais; (iii) Nova classe poderá receber testes e melhorias independentes. Consequências: menos Operações no método cliente; menos Responsabilidades da classe cliente; mais Classes; mais Acoplamento da classe cliente com a nova classe.

\vspace{-0.5em}
\section{Dos conceitos de código limpo para as métricas de código-fonte}
\vspace{-0.3em}

Métricas de código-fonte nos permitem criar mecanismos automatizáveis para detecção de características obtidas através da análise do código-fonte. Entretanto, diante do vasto conjunto existente de métricas, usá-las de forma isolado e compreender os significados dos valores de todas é uma tarefa custosa~\cite{Fenton1999}, o que pode levar ao pouco uso de métricas de código-fonte de maneira geral.
%                               
Dada a dificuldade em determinar parâmetros numéricos para as métricas de código-fonte. Neste trabalho, propomos uma abordagem baseada em cenários para identificar trechos de código com características indesejáveis. Nesses cenários (``problemáticos''), elaboramos contextos criados a partir de poucos conceitos de código limpo no qual um pequeno conjunto de métricas é analisado e interpretado através da combinação de seus resultados. Por exemplo, conceitualmente buscamos métodos pequenos e com apenas uma tarefa. Sendo assim, estes cenários também contextualizam a possível interpretação de métricas de código-fonte, com por exemplo, relativas ao número de linhas e quantidade de estruturas de controle de forma que seus valores indiquem a presença de métodos grandes.
                         
O mapeamento desenvolvido nesta trabalho não pretende afirmar categoricamente se um código é limpo ou não. O objetivo é facilitar melhorias de implementação através da aproximação dos valores das métricas com os esperados nos contextos de interpretação. O mapeamento proposto neste trabalho visa facilitar a procura por problemas quanto a limpeza do código. Cada cenário é descrito através dos seguintes componentes:
%
(i. \textbf{Conceitos de limpeza relacionados}) conceitos de limpeza de código que motivaram a criação do cenário;
%
(ii. \textbf{Características}) indicam a presença de falhas em relação as referências apresentadas;
%
(iii. \textbf{Métricas}) mecanismos que permitem analisar o código a procura das características do cenário;
%
(iv. \textbf{Objetivo durante a refatoração}) quais devem ser as ideias principais durante a refatoração para eliminar o problema;
%
(v. \textbf{Resultados esperados}) quais características devem ser encontradas no código quando terminar a refatoração para eliminação do cenário.

Quando algum cenário é detectado, sugerimos que o usuário analise o trecho indicado para verificar se é possível refatorar o código e melhorar sua limpeza. Esse mapeamento não visa apenas afirmar se existem ``problemas'' no código, mas sim indicar partes dele que talvez possam ser melhoradas de acordo com os conceitos que definimos como código limpo.
%
Para facilitar a avaliação do código ao longo de seu desenvolvimento, a interpretação dos valores das métricas deve simples. Esperamos que a preocupação com a ``limpeza do código'' não emerja apenas quando ele estiver pronto, pois nessa fase é provável que ele tenha vários problemas e que seja muito complicado alterá-lo. Por esse motivo, montamos alguns cenários e escolhemos um conjunto pequeno de métricas que nos permite detectar as características procuradas.

Em suma, os conceitos de código limpo que apresentamos foram mapeados para o seguinte conjunto de métricas, baseado em \cite{Marinescu02} e \cite{Lanza06}: Número de Chamadas (NC); Número de Chamadas Externas (NEC); Taxa de Chamadas Externas (ECR); Número de Classes Chamadas (NCC); Número de Atributos Alcançáveis (NRA); Nível Máximo de Estruturas Encadeadas (MaxNesting); Complexidade Ciclomática (Cyclo); Número de Parâmetros (NP); Número de Repasses de Parâmetros (NRP); Número de Linhas (LOC); Número de Atributos (NOA); Número de Métodos (NOM); Soma do Número de linhas (SLOC); Média do Número de Linhas Por Método (AMLOC); Falta de Coesão Entre Métodos (LCOM4). Até o momento, como limitação deste trabalho, não conseguimos mapear todos os conceitos de código limpo usando métricas. Por exemplo, não consguimos encontrar problemas relacionados a nomes pouco expressivos usando apenas métricas. Nesse caso, não é suficiente analisarmos somente a estrutura do código: precisamos entender o contexto em que cada nome é usado, e para isso, também é necessária uma análise semântica.

\subsection{Cenários para aplicação das técnicas de código limpo}

\paragraph*{-- Método Grande.} Neste cenário, os conceitos de código limpo tratados são: Composição de Métodos, a necessidade de Evitar Estruturas Encadeadas e a possibilidade de usar Cláusulas Guarda. Nesse contexto, métodos grandes tem como principal característica (i) um elevado número de linhas, ou seja, um alto valor de LOC. É comum também possuir (ii) um elevado número de quebras condicionais de fluxo, indicado por um valor alto da CYCLO e (iii) profundas estruturas com condicionais encadeados, indicadas por um valor alto do MaxNesting.
%                                          
Uma refatoração sugere diminuir o número de linhas (LOC), diminuir a complexidade ciclomática (CYCLO) e eliminar as estruturas encadeadas (MaxNesting) no método em análise. Conseguimos atingir esses objetivos decompondo o método grande em métodos menores. Normalmente, começamos essa decomposição passando os blocos de código dos desvios de fluxo para outros lugares, pois esses são conjuntos de operações evidentemente isoladas do resto do método.
%                                            
Com isso, esperamos que a média de linhas por método (média de LOC) e as profundidades máximas de estruturas encadeadas de cada método (MaxNesting) abaixem. Provavelmente, a soma da complexidade ciclomática da classe (CYCLO) não sofrerá alterações. Isso acontece porque normalmente espalhamos essas quebras de fluxo em outros métodos da mesma classe, mas não as eliminamos da sua lógica. Teremos uma redução da complexidade ciclomática quando parte do código for eliminado ou trasferido para métodos de outras classes. Além disso, pode acontecer um aumento no número de métodos da classe, um aumento do número de métodos de outras classes ou até a criação de novas classes.

\vspace{-1.7em}	
\paragraph*{-- Método com Muitos Fluxos Condicionais.} Os conceitos de código limpo que constituem este cenário são a Composição de Métodos, Evitar Estruturas Encadeadas e o Uso de Exceções. Esse cenário ocorre quando temos métodos que são complexos, mas não necessariamente grandes. Suas principais características são: (i) muitas quebras condicionais de fluxo (valor alto de CYCLO) e (ii) longas estruturas com condicionais encadeados (valor alto de MaxNesting).
%                                                                          
Em métricas, queremos minimizar a complexidade ciclomática (CYCLO) e a profundidade máxima de estruturas encadeadas (MaxNesting) do método, pois queremos deixar o código mais simples e direto. Podemos nos basear na decomposição de métodos para atingirmos nossos objetivos.
%
Depois das modificações, esperamos que a profundidade máxima de estruturas encadeadas tenha diminuído (redução do MaxNesting). Quando a refatoração for baseada no uso de exceções, como \textit{try catch} é provável que a complexidade ciclomática do método não se altere, pois essas estruturas também são contadas como desvios de fluxo no calculo da CYCLO. Pode acontecer também um aumento no número de métodos da classe, porque, em alguns casos, o conteúdo do bloco de cada desvio condicional é deslocado para novos métodos.

\vspace{-1.7em}
\paragraph*{-- Método com Muitos Parâmetros.} Este cenário tem como base a técnica do uso de Objeto com Parâmetro. A principal característica de um método que está nesse contexto é ter um elevado número de parâmetros (valor alto de NP). Objetivo é refatorar para minimizar o número de parâmetros recebidos (diminuir o NP). Os resultados esperados após a refatoração são a redução do número de parâmetros (NP) e o aumento do número de classe. Esse aumento ocorre quando criamos classes para agrupar os parâmetros que pertencem a um mesmo contexto em um único objeto.

\vspace{-1.7em}	                                               
\paragraph*{-- Muita Passagem de Parâmetros Pela Classe.} Os conceitos de código limpo que constituem este cenário são o uso de Objeto como Parâmetro e a transformação de parâmetros em variáveis de instância. Podemos separar esse cenário em dois casos diferentes. O primeiro trata do repasse de muitos parâmetros. Sua característica é ter uma elevada média de parâmetros repassados (alta média de NRP). Parâmetros repassados são aqueles recebidos por um método que os repassam em chamadas a operações. O segundo caso se preocupa com a elevada média do número de parâmetros por método da classe analisada (alta média de NP).       
%		     
Então, é comum que os métodos de classes com muita passagem de parâmetro tenham muitos ou repassem muitos parâmetros, ou recebam variáveis que são usadas apenas em chamadas a outros métodos. Em termos de métricas, os objetivos são diminuir o número de repasses de parâmetros pela classe (diminuir o NRP) e reduzir o número de parâmetros dos métodos (reduzir o NP). Fazemos isso transformando os parâmetros muito repassados ou usados em vários métodos em atributos e criando objetos para guardar conjuntos de parâmetros relacionados. Com isso, temos a redução da média do número de parâmetros por método (redução da média de NP), redução do número de repasses de parâmetros (redução do NRP) e o aumento do número de variáveis de instância. Além disso, pode ocorrer um aumento no número de classes caso aconteça a junção de variáveis em um único objeto.	           

\vspace{-1.7em}
\paragraph*{-- Método ``Invejoso''.} Método ``invejoso'' é aquele que usa um elevado número de métodos e atributos de poucas classes não relacionadas hierarquicamente com a sua própria. Mapeando essas características em métricas de código-fonte, temos neste cenário alta ECR, que indica a taxa de chamadas a métodos e atributos externos, e baixa NCC, que representa o número de classes não relacionadas chamadas pelo método.
%
A preocupação durante uma refatoração neste contexto deve ser a minimização do número de chamadas externas, calculado através do NEC. O conceito de código limpo que constitui este cenário é a Delegação de Tarefa. Após a refatoração, esperamos uma redução do número de chamadas externas (diminuição do NEC), da taxa de chamadas externas (diminuição do ECR) e da média do número de chamadas externas da classe (diminuição da média de NEC).

\vspace{-1.7em}
\paragraph*{-- Método Dispersamente Acoplado.} Um método dispersamente acoplado utiliza um elevado número de atributos e métodos de várias classes não relacionadas hierarquicamente com a sua. Assim como no contexto de método ``invejoso'', neste cenário temos um valor alto de ECR, indicando que o uso de métodos e atributos externos é maior do que de internos. Porém, a diferença entre eles é o número de classes não relacionadas chamadas pelo método, sendo o valor do NCC alto neste cenário e baixo para métodos ``invejosos''.
%
Durante uma refatoração, devemos focar na redução do número de chamadas externas (NEC). Os conceitos de código limpo que constituem este cenário são o de Objeto Centralizador e o de Objeto Método. Os resultados esperados após uma refatoração são a redução do número de chamadas externas e de sua média na classe do método analisado (NEC e média de NEC) e a diminuição do número de classes não relacionadas acessadas (NCC). Também pode ocorrer um aumento no número de classes. Isso acontecerá quando uma classe for criada para centralizar o diálogo entre as classes não relacionadas, sendo sacrificada para que as outras mantenham-se limpas e reutilizáveis em outros projetos.  

\vspace{-1.7em}
\paragraph*{-- Classe Pouco Coesa.} Os objetivos são a maximização da Coesão e atingir o Princípio da Responsabilidade Única.
%
A principal característica de uma classe pouco coesa é possuir subdivisões em grupos de métodos e atributos que não se relacionam (valor de LCOM4 maior que 1), ou possuir métodos que alcançam em média poucos atributos da propria classe (média de NRA muito menor do que o valor de NOA). Nesse contexto, dizemos que um método alcança um atributo se ele usa o atributo no seu próprio corpo ou de forma indireta, ou seja, através da chamada de algum método que usa o atributo direta ou indiretamente.
%   
Os objetivos durante uma refatoração deste cenário são aumentar a coesão (diminuir a LCOM4) e diminuir a diferença entre média do número de atributos alcançáveis (NRA) e o número de atributos da classe analisada (NOA). Portanto, uma boa refatoração neste contexto seria separar as subdivisões já existentes na classe analisada em classes mais coesas. Como resultado da refatoração deste cenário, esperamos encontrar um aumento do número de classes. Além disso, desejamos que as classes existentes não tenham subdivisões (LCOM4 = 1) e que a média do número de atributos alcançáveis por cada método seja próxima da quantidade de atributos da classe (média de NRA próxima de NOA).

\vspace{-0.5em}
%-------------------------------------------------------------------------------
\section{Consideração finais}
\vspace{-0.3em}

Apresentamos um conjunto de conceitos relacionados a um código limpo e
seu mapeamento em métricas de código-fonte com o objetivo de prover aos desenvolvedores uma maneira
de pensarem em melhorias para os seus códigos.
%
Em suma, compilamos e abordamos de forma prática os conceitos sobre código limpo do ponto de vista de renomados especialistas em desenvolvedores de software. 

Um estudo de caso para validar esse mapeamento não está descrito neste artigo, mas pode ser encontrado em nosso relatório técnico (\textit{http://ccsl.ime.usp.br/codigolimpo}) -- refatoramos o software livre para cálculo de métricas de código-fonte ``Analizo''(\textit{http://analizo.org}). Entre os resultados, podemos destacar uma similariedade na sequência da detecção dos cenários e suas respectivas melhorias.

Em um primeiro momento, frequentemente detectamos a presença dos cenários Método Grande e Métodos com Muitos 
Fluxos Condicionais, expressos pelo LOC, CYCLO e MaxNesting dos métodos. Seguindo os conceitos de código limpo,
diante desse contexto, uma melhoria possível é utilizar a \textit{Composição de Métodos} buscando
\textit{Evitar as Estruturas Encadeadas}.
%
O segundo ``estado'' recorrente durante as alterações é a detecção de \textit{Métodos com Muitos Parâmetros} e
\textit{Muita Passagem de Parâmetros pela Classe}. Para que as tarefas possam ser divididas pelos métodos 
criados na \textit{Composição de Métodos}, muitos parâmetros são necessários e frequentemente encontramos
métodos que simplesmente repassam os mesmos.Para minimizar tais cenários, podemos promover alguns parâmetros para 
variáveis de instância.

Seguindo as ``técnicas'' de código limpo ao longo das alterações, a partir do momento em que novas variáveis de instância
são adicionadas, frequentemente detectamos a queda na coesão da classe em questão. Nesse momento podemos identificar
mais facilmente as responsabilidades que a classe concentrava de uma maneira mais próxima da implementação.
Desta forma, provavelmente existem conjuntos de métodos e variáveis que não se relacionam, causando um valor
LCOM4 maior do que 1, ou métodos que trabalham apenas sobre uma parcela das
variáveis, o quer resulta em baixa média de NRA em relação ao número de variáveis de instância (NOA).
%
Sendo assim, geralmente optamos por quebrar a classe com baixa coesão. A medida para essa divisão capaz de
gerar classes extremamente coesas são as responsabilidades as quais encapsulam. Sabemos que classes com uma
responsabilidade geralmente possuem LCOM4 igual a 1, mas o inverso não pode ser afirmado.

Do ponto de vista do mapamento dos conceitos em métricas de código-fonte, o uso
de cenários é capaz de detectar trechos de código que poderiam receber melhorias.
%
O que podemos concluir ao final desse estudos é que, nos exemplos de código, estudo de caso e conceitos da bibliografia sobre os quais trabalhamos,
para cada método buscamos minimizar os valores das métricas LOC, NP, NRP, CYCLO e MaxNesting, além de criar
classes que minimizem LCOM4 e a diferença entre a média de NRA e NOA. As alterações nos valores dessas métricas, ou seja, o controle e monitoramento delas, apontaram para um ``código mais limpo'', de acordo com os conceitos apresentados.

\vspace{-0.5em}
%\scalefont{0.95}
\bibliographystyle{sbc}
\bibliography{referencias}
\end{document}
% vim: ts=2 sw=2 expandtab
